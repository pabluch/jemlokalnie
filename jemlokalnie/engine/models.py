﻿# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
import sys, os
import datetime
import dateutil.parser

from django.conf import settings
from django.db import models
from django.db.models import Q, F, Count, signals
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy
from django.template.defaultfilters import slugify
from unidecode import unidecode

from taggit.managers import TaggableManager
from location_field.models.plain import PlainLocationField
from os.path import split
logger = logging.getLogger(settings.DEFAULT_LOGGER)

from PIL import Image, ImageOps

class FoodGeneralist(models.Model):

    name = models.CharField(max_length=100, blank=False, verbose_name="Nazwa wyświetlana")
    slug = models.SlugField(null=True, blank=True)
    tags = TaggableManager( blank = True, verbose_name="Słowa kluczowe",help_text=_("Słowa kluczowe opisujące twoją działalność oddzielone przecinkami np.: sery, wędliny, warzywa, owoce, przetwory, restauracja, kuchnia polska (wpisz przynajmniej trzy)"))
    short = models.CharField(max_length=256, blank=False, verbose_name="Któtki opis")
    long = models.TextField(blank=False, verbose_name="Pełny opis")

    adress = models.CharField(max_length=512, default="Polska,", verbose_name=_("Adres"))
    location = PlainLocationField(based_fields=['adress'], zoom=7, blank =True, verbose_name=_("Mapa"))

    def __unicode__(self):
        return "%s"%(self.name) or u'(brak)'

    def save(self, *args, **kwargs):
        self.slug = slugify( unidecode(self.name))
        super(FoodGeneralist, self).save(*args, **kwargs)

    class Meta:
        abstract = True

    @property
    def picon(self):
        return 'pIcon1'

    @property
    def ready(self):
        if self.name is not None and self.short is not None \
        and self.tags.count() >1 and self.of_user.is_active:
            return True
        return False

    @property
    def contact(self):
        return self.of_user.contact


def user_directory_path(instance, filename):
    return 'u_{0}/{1}'.format(instance.img.of_user.id, filename)

class FoodImg(models.Model):
    img_f = models.ImageField(upload_to=user_directory_path, null = True, verbose_name="Zdjęcie")
    max_wh = 1024
    thumb_wh = 200

    @property
    def thumb_path(self):
        p,f = split(self.img_f.path)
        return p+"/thumb_"+f

    @property
    def thumb_url(self):
        p,f = split(self.img_f.url)
        return p+"/thumb_"+f
    
    @property
    def url(self):
        return self.img_f.url

    def save(self, *args, **kwargs):
        super(FoodImg, self).save(*args, **kwargs)
        img = Image.open(self.img_f.path)
        exif = img._getexif()
        ORIENTATION = 274
        if exif is not None and ORIENTATION in exif:
            orientation = exif[ORIENTATION]
            method = {2: Image.FLIP_LEFT_RIGHT, 4: Image.FLIP_TOP_BOTTOM, 8: Image.ROTATE_90, 3: Image.ROTATE_180, 6: Image.ROTATE_270, 5: Image.TRANSPOSE}
            if orientation in method:
               img = img.transpose(method[orientation])

        if img.height > self.max_wh or img.width > self.max_wh:
            img.thumbnail((self.max_wh, self.max_wh))
            img.save(self.img_f.path)

        os.chmod(self.img_f.path, 0644)
        thumb = ImageOps.fit(img, (self.thumb_wh, self.thumb_wh), Image.ANTIALIAS)
        thumb.save(self.thumb_path)

    class Meta:
        abstract = True

class FoodProvider( FoodGeneralist ):

    my_distributors = models.ForeignKey('FoodDistributor', null=True, default = None, on_delete = models.SET_NULL, blank=True )
    of_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, default = None, on_delete = models.SET_NULL )

    @property
    def url(self):
        return reverse_lazy('website:producent', kwargs={ 'name':self.slug})
   
    @property
    def color(self):
        return '#45b648'

    @property
    def images(self):
        return [{'img': x.url, 'thumb':x.thumb_url} for x in self.foodimgprovider_set.all()]

    class Meta:
        verbose_name        = _('Producent żywności')
        verbose_name_plural = _('Producenci żywności')

class FoodImgProvider(FoodImg):
    img = models.ForeignKey(FoodProvider, null=True, default = None, on_delete = models.SET_NULL, blank=True)


class FoodDistributor( FoodGeneralist ):

    my_providers = models.ForeignKey(FoodProvider, null=True, default = None, on_delete = models.SET_NULL, blank=True )
    of_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, default = None, on_delete = models.SET_NULL )

    class Meta:
        verbose_name        = _('Restauracja')
        verbose_name_plural = _('Restauracje')

    @property
    def images(self):
        return [{'img': x.url, 'thumb':x.thumb_url} for x in self.foodimgdistr_set.all()]

    @property
    def url(self):
        return reverse_lazy('website:restauracja', kwargs={ 'name':self.slug})

    @property
    def picon(self):
        return 'pIcon2'

    @property
    def color(self):
        return '#ffde00'


class FoodImgDistr(FoodImg):
    img = models.ForeignKey(FoodDistributor, null=True, default = None, on_delete = models.SET_NULL, blank=True)


class FoodDistributorShop( FoodGeneralist ):

    my_providers = models.ForeignKey(FoodProvider, null=True, default = None, on_delete = models.SET_NULL, blank=True )
    of_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, default = None, on_delete = models.SET_NULL )

    class Meta:
        verbose_name        = _('Sklep')
        verbose_name_plural = _('Sklepy')

    @property
    def url(self):
        return reverse_lazy('website:sklep', kwargs={ 'name':self.slug})

    @property
    def images(self):
        return [{'img': x.url, 'thumb':x.thumb_url} for x in self.foodimgdistrshop_set.all()]

    @property
    def color(self):
        return '#ff6600'

    @property
    def picon(self):
        return 'pIcon3'

class FoodImgDistrShop(FoodImg):
    img = models.ForeignKey(FoodDistributorShop, null=True, default = None, on_delete = models.SET_NULL, blank=True)
