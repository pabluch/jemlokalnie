from django.apps import AppConfig


class EngineConfig(AppConfig):
    name = 'jemlokalnie.engine'
    verbose_name = "Engine"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
