﻿from django.conf.urls import url
from django.views.generic import TemplateView
from allauth.account.views import SignupView
from . import views

urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name='website/home_intro.html'), name='home'),
    url(r'^$', views.HomeView.as_view(), name='home'),
    # url(r'^dolacz$', TemplateView.as_view(template_name='website/signup_intro.html'), name='signup_intro'),
    url(r'^dolacz$', SignupView.as_view(), name='signup_intro'),
    url(r'^dolacz/$', SignupView.as_view(), name='contact_form'),
    url(r'^onas$', TemplateView.as_view(template_name='website/home_about.html'), name='about'),
    url(r'^restauracje$', views.FoodDistributorsView.as_view(), name='restauracje'),
    url(r'^sklepy$', views.FoodDistributorsShopView.as_view(), name='sklepy'),
    url(r'^wytworcy$', views.FoodProvidersView.as_view(), name='producenci'),
    url(
        regex=r'^wytworcy/(?P<name>[\w.@+-]+)/$',
        view=views.FoodProviderView.as_view(),
        name='producent'
    ),
    url(
        regex=r'^restauracje/(?P<name>[\w.@+-]+)/$',
        view=views.FoodDistributorView.as_view(),
        name='restauracja'
    ),
    url(
        regex=r'^sklepy/(?P<name>[\w.@+-]+)/$',
        view=views.FoodDistributorShopView.as_view(),
        name='sklep'
    ),
]

