# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging

from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, TemplateView
from django.forms.models import inlineformset_factory
from django import forms

from django.contrib.auth.mixins import LoginRequiredMixin

from django.utils.translation import ugettext_lazy as _

from jemlokalnie.engine.models import FoodDistributor, FoodProvider, FoodDistributorShop

logger = logging.getLogger(settings.DEFAULT_LOGGER)

class FoodProviderView( DetailView):
    model = FoodProvider
    slug_field = 'slug'
    slug_url_kwarg = 'name'
    template_name = "website/foodprovider.html" 

    def get_context_data( self, **kwargs):
        context = super( DetailView, self).get_context_data(**kwargs)
        context['images'] = context['object'].images
        context['contact'] = self.object.contact
        return context

class FoodProvidersView( ListView ):
    model = FoodProvider
    template_name = "website/foodprovider_list.html"

    def get_context_data( self, **kwargs):
        context = super( ListView, self).get_context_data(**kwargs)
        context['the_name'] = self.model._meta.verbose_name_plural
        context['object_list'] = filter(lambda x: x.ready, context['object_list'])
        return context


class FoodDistributorView( FoodProviderView):
    model = FoodDistributor

class FoodDistributorsView( FoodProvidersView ):
    model = FoodDistributor
    template_name = "website/foodprovider_list.html"	


class FoodDistributorShopView( FoodProviderView):
    model = FoodDistributorShop

class FoodDistributorsShopView( FoodProvidersView ):
    model = FoodDistributorShop
    template_name = "website/foodprovider_list.html"


class HomeView(TemplateView):
    template_name = "website/home.html"

    def get_context_data( self, **kwargs):
        context = super( HomeView, self).get_context_data(**kwargs)
        context['object_list']=[]
        context['object_list']+=FoodDistributor.objects.all();
        context['object_list']+=FoodDistributorShop.objects.all();
        context['object_list']+=FoodProvider.objects.all();

        context['object_list'] = filter(lambda x: x.ready, context['object_list'])
        return context  