﻿# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User
from jemlokalnie.engine.models import FoodProvider, FoodDistributor, FoodDistributorShop, FoodImgDistrShop, FoodImgProvider, FoodImgDistr
from allauth.account.models import EmailAddress

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

class EmailAddressInline(admin.StackedInline):
    model = EmailAddress
    extra = 0

class FoodImgDistrInline(admin.StackedInline):
    model = FoodImgDistr
    extra = 1

class FoodImgProviderInline(admin.StackedInline):
    model = FoodImgProvider
    extra = 1

class FoodImgDistrShopInline(admin.StackedInline):
    model = FoodImgDistrShop
    extra = 1

class FoodDistributorInline(admin.StackedInline):
    model = FoodDistributor
    extra = 0
    inlines = [ FoodImgDistrInline, ]

class FoodDistributorShopInline(admin.StackedInline):
    model = FoodDistributorShop
    extra = 0
    inlines = [ FoodImgDistrShopInline, ]

class FoodProviderInline(admin.StackedInline):
    model = FoodProvider
    extra = 0
    inlines = [ FoodImgProviderInline, ]


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
            ('User Profile', {'fields': ('name','user_type',
                                         'deal_accepted', 'deal_sent', 'rules_accepted',
                                         'phone','full_name', 'tax_id', 'phisical_address')}),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username', 'name','phone','user_type', 'is_active','deal_url','page_url','date_joined')
    search_fields = ['name']
    inlines = [
                EmailAddressInline,FoodProviderInline,FoodDistributorInline, FoodDistributorShopInline, 
                ]



@admin.register(FoodDistributor)
class AdminFoodDistributor(admin.ModelAdmin):
    inlines = [ FoodImgDistrInline ]

@admin.register(FoodProvider)
class AdminFoodProvider(admin.ModelAdmin):
    inlines = [ FoodImgProviderInline ]

@admin.register(FoodDistributorShop)
class AdminFoodDistributorShop(admin.ModelAdmin):
    inlines = [ FoodImgDistrShopInline ]


@admin.register(FoodImgProvider)
class AdminFoodImgProvider(admin.ModelAdmin):
    pass
