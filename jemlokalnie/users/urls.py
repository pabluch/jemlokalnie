from django.conf.urls import url

from . import views

# usermatch = "[\w.@+-]+"
usermatch = ".+"
urlpatterns = [
    url(
        regex=r'^umowa/(?P<username>{})/$'.format(usermatch),
        view=views.UserListViewDeal.as_view(),
        name='umowa'
    ),
    url(
        regex=r'^qr/(?P<username>{})/$'.format(usermatch),
        view=views.UserListViewQR.as_view(),
        name='qr'
    ),
    url(
        regex=r'^polecwytworce/(?P<username>{})/$'.format(usermatch),
        view=views.UserAddProviderDistributor.as_view(),
        name='polecwytworce'
    ),
    url(
        regex=r'^polecrestautacje/(?P<username>{})/$'.format(usermatch),
        view=views.UserAddProviderDistributor.as_view(),
        name='polecrestautacje'
    ),
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),
    url(
        regex=r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),
    url(
        regex=r'^~update/$',
        view=views.UserUpdateView.as_view(),
        name='update'
    ),
    url(
        regex=r'^(?P<username>{})/$'.format(usermatch),
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
]

# [\w.@+- ]