﻿# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from itertools import chain
import logging

from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, TemplateView
from django.forms.models import inlineformset_factory
from django import forms
from django.core.exceptions import SuspiciousOperation

from django.contrib.auth.mixins import LoginRequiredMixin

from django.utils.translation import ugettext_lazy as _

from .models import User
from jemlokalnie.engine.models import FoodDistributor, FoodProvider, FoodDistributorShop, \
                                      FoodImgDistr, FoodImgProvider, FoodImgDistrShop

from pprint import pprint as pp

logger = logging.getLogger(settings.DEFAULT_LOGGER)

FoodDistributorFormSet      = inlineformset_factory(User, FoodDistributor, extra=0, can_delete = False, 
                                fields = ["name","tags","adress","location","short","long"])
FoodProviderFormSet         = inlineformset_factory(User, FoodProvider, extra=0, can_delete = False, 
                                fields = ["name","tags","adress","location","short","long"])
FoodDistributorShopFormSet  = inlineformset_factory(User, FoodDistributorShop, extra=0, can_delete = False, 
                                fields = ["name","tags","adress","location","short","long"])

FoodProviderImgFormSet  = inlineformset_factory(FoodProvider, FoodImgProvider, extra=1, can_delete = True, fields = ["img_f",])
FoodDistrImgFormSet     = inlineformset_factory(FoodDistributor, FoodImgDistr, extra=1, can_delete = True, fields = ["img_f",])
FoodDistrShopImgFormSet = inlineformset_factory(FoodDistributorShop, FoodImgDistrShop, extra=1, can_delete = True, fields = ["img_f",])

class UserDetailFomMixin(object):
    def add_user_type_formset(self, context):
        context['user_type']  = dict(User.TYPES)[self.object.user_type];
        if self.request.POST:
            if self.object.user_type in (User.TYPES_FD, ):
                context['fd_form']  = FoodDistributorFormSet(self.request.POST, instance=self.object)
                context['fd_iform'] = FoodDistrImgFormSet(self.request.POST, self.request.FILES, instance=self.object.fooddistributor_set.first())
            if self.object.user_type in (User.TYPES_FP, ):
                context['fp_form']  = FoodProviderFormSet(self.request.POST, instance=self.object)
                context['fp_iform'] = FoodProviderImgFormSet(self.request.POST, self.request.FILES, instance=self.object.foodprovider_set.first())
            if self.object.user_type in (User.TYPES_FDS, ):
                context['fs_form']  = FoodDistributorShopFormSet(self.request.POST, instance=self.object)
                context['fs_iform'] = FoodDistrShopImgFormSet(self.request.POST, self.request.FILES, instance=self.object.fooddistributorshop_set.first())
        else:
            if self.object.user_type in (User.TYPES_FD, ):
                context['fd_form']  = FoodDistributorFormSet(instance=self.object)
                context['fd_iform'] = FoodDistrImgFormSet(instance=self.object.fooddistributor_set.first())
            if self.object.user_type in (User.TYPES_FP, ):
                context['fp_form']  = FoodProviderFormSet(instance=self.object)
                context['fp_iform'] = FoodProviderImgFormSet(instance=self.object.foodprovider_set.first())
            if self.object.user_type in (User.TYPES_FDS, ):
                context['fs_form']  = FoodDistributorShopFormSet(instance=self.object)
                context['fs_iform'] = FoodDistrShopImgFormSet(instance=self.object.fooddistributorshop_set.first())
        return context

class UserDetailView(LoginRequiredMixin, UserDetailFomMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'

    def get_context_data( self, **kwargs):
        context = super( UserDetailView, self).get_context_data(**kwargs)

        self.add_user_type_formset(context)
        context['phone'] = self.object.phone;

        context['my_sites'] = []
        for obj in chain(self.object.foodprovider_set.all(),
                         self.object.fooddistributor_set.all(),
                         self.object.fooddistributorshop_set.all()):
            context['my_sites'].append(obj.url)
        return context


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})



class UserUpdateView(LoginRequiredMixin, UserDetailFomMixin, UpdateView):
    fields = ['phone']
    model = User

    def get_success_url(self):
        return reverse('users:detail', kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)

    def get_context_data( self, **kwargs):
        context = super( UserUpdateView, self).get_context_data(**kwargs)
        self.add_user_type_formset(context)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        inline_forms = [(x,context[x]) for x in ['fp_form','fd_form','fs_form','fp_iform','fd_iform','fs_iform'] if x in context]
        logger.debug(inline_forms)

        if all(x[1].is_valid() for x in inline_forms ):
            self.object = form.save()
            for x in inline_forms:
                if x[0] == 'fp_iform':
                    x[1].instance = self.object.foodprovider_set.first()
                elif x[0] == 'fd_iform':
                    x[1].instance = self.object.fooddistributor_set.first()
                elif x[0] == 'fs_iform':
                    x[1].instance = self.object.fooddistributorshop_set.first()
                else:
                    x[1].instance = self.object
                x[1].save()
            return super(UserUpdateView, self).form_valid(form)

        return self.render_to_response(self.get_context_data(form=form))

class UserListViewDeal(LoginRequiredMixin, UserDetailFomMixin, DetailView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name = "users/deal_1.html"

    def get_context_data( self, **kwargs):
        context = super( DetailView, self).get_context_data(**kwargs)
        if self.request.user.username != context['user'].username and not self.request.user.is_staff:
            raise SuspiciousOperation("Przykro nam ale ta strone nie jest dla Państwa dostępna. Incydent został zalogowany")

        return context

class UserListViewQR(LoginRequiredMixin, UserDetailFomMixin, DetailView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name = "users/qr.html"

    def get_context_data( self, **kwargs):
        context = super( DetailView, self).get_context_data(**kwargs)

        self.add_user_type_formset(context)
        context['phone'] = self.object.phone;

        context['my_sites'] = []
        for obj in chain(self.object.foodprovider_set.all(),
                         self.object.fooddistributor_set.all(),
                         self.object.fooddistributorshop_set.all()):
            context['my_sites'].append(obj.url)

        context['contact'] = self.object.contact
        return context

class UserListView(LoginRequiredMixin, ListView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'

class UserAddProviderDistributor(UserDetailView):
    template_name = "users/user_add_more.html"
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'

class JemlokalnieSignupForm( forms.Form ):
    user_type = forms.ChoiceField( choices = User.TYPES, label=_('Rodzaj aktywności'))
    rules_accepted =  forms.BooleanField( label=_('Akceptuję regulamin oraz zasady ochrony danych osobowych (RODO)'), required = True )
    deal_accepted =  forms.BooleanField( label=_('Potwierdzam zawarcie umowy'), required = False )
    full_name = forms.CharField( label=_('Imię i nazwisko'), required = True ,help_text="Imię i nazwisko lub nazwa firmy.")
    tax_id = forms.CharField( label=_('NIP/PESEL'), required = False ,help_text="Podaj jeżeli chcesz przystąpić do programu JemLokalnie.")
    phisical_address = forms.CharField( label=_('Adres korespondencyjny'), required = False,help_text="Podaj jeżeli chcesz przystąpić do programu JemLokalnie")
    phone = forms.CharField( label=_('Numer telefonu'), required = False )

    def clean_rules_accepted(self):
        data = self.cleaned_data['rules_accepted']

        if not data:
            raise forms.ValidationError("Proszę zaakceptować regulamin i potwierdzić zapoznanie się z RODO")
        return data

    def clean_deal_accepted(self):
        data = self.cleaned_data['deal_accepted']
        active_user_type = self.cleaned_data['user_type'] != '4'

        if active_user_type and not data:
            raise forms.ValidationError("Proszę zaakceptować umowę")
        return data

    def clean_tax_id(self):
        data = self.cleaned_data['tax_id']
        active_user_type = self.cleaned_data['user_type'] != '4'

        if active_user_type:
            if not data:
                raise forms.ValidationError("Dla zaznaczonego rodzaju aktywności w Programie to pole jest wymagane.")
            if len(data) < 7:
                raise forms.ValidationError("Proszę wpisz więcej.")
        return data

    def clean_phisical_address(self):
        data = self.cleaned_data['phisical_address']
        active_user_type = self.cleaned_data['user_type'] != '4'
        
        if active_user_type:
            if not data:
                raise forms.ValidationError("Dla zaznaczonego rodzaju aktywności w Programie to pole jest wymagane.")
            if len(data) < 17:
                raise forms.ValidationError("Proszę wpisz więcej.")
        return data

    def clean_full_name(self):
        data = self.cleaned_data['full_name']
        active_user_type = self.cleaned_data['user_type'] != '4'

        if active_user_type:
            if not data:
                raise forms.ValidationError("Dla zaznaczonego rodzaju aktywności w Programie to pole jest wymagane.")
            if len(data) < 3:
                raise forms.ValidationError("Proszę wpisz więcej.")
        return data

    def signup(self, request, user):
        user.rules_accepted = self.cleaned_data['rules_accepted']
        user.deal_accepted = self.cleaned_data['deal_accepted']
        user.user_type = self.cleaned_data['user_type']

        user.phone = self.cleaned_data['phone']
        user.full_name = self.cleaned_data['full_name']
        user.phisical_address = self.cleaned_data['phisical_address']
        user.tax_id = self.cleaned_data['tax_id']
        user.save()

    class Meta:
        #model = mymodel.CustomModel
        fields = [
            'user_type', 'rules_accepted', 'phone'
        ]

from allauth.account.forms import LoginForm
class JemlokalnieLoginForm(LoginForm):
    pass        