# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models

from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.validators import ASCIIUsernameValidator 
from django.utils.safestring import mark_safe

from jemlokalnie.engine.models import FoodDistributor, FoodProvider

logger = logging.getLogger(settings.DEFAULT_LOGGER)

class UsernameValidator(ASCIIUsernameValidator ):
    regex = r'^[\s\w\@\+\-]+$'


@python_2_unicode_compatible
class User(AbstractUser):
    TYPES_FD = 2
    TYPES_FDS = 3
    TYPES_FP = 1
    TYPES_FD_FP = 5
    TYPES_FP_FD = 5
    TYPES_RU = 4
    TYPES   =  (
        (1, _('Producent żywności | Wytwórca | Przetwórca')), 
        (3, _('Sklep')),
        (2, _('Restaurator')),
        (4, _('Klient | Odbiorca')),
    )
    username_validator = UsernameValidator()
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
            error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    name = models.CharField(_('Name of User'), blank=True, max_length=255, 
                help_text=_('Proszę podać uproszczoną nazwę, pełną nazwę będzie można uzypełnić po zarejestrowaniu'))
    rules_accepted =  models.BooleanField( default = False, verbose_name=_('Akceptuję regulamin') )
    deal_accepted =  models.BooleanField( default = False, verbose_name=_('Akceptuję umowę') )
    deal_sent =  models.BooleanField( default = False, verbose_name=_('Umowa przesłana') )
    user_type = models.IntegerField( default = 1, choices = TYPES, verbose_name=_('Rodzaj aktywności'))
    phone = models.CharField(max_length=50 , verbose_name=_('Numer telefonu'), null=False, blank=False )
    
    full_name = models.CharField(max_length=512 , verbose_name=_('Imię i nazwisko'), null=False, blank=True )
    phisical_address = models.CharField(max_length=512 , verbose_name=_('Adres zamieszkania'), null=False, blank=True )
    tax_id = models.CharField(max_length=512 , verbose_name=_('NIP/PESEL'), null=False, blank=True )

    def __str__(self):
        return self.username

    @property
    def deal_url(self):
        return mark_safe("<a href='{}{}' target='_blank'>umowa</a>".format(settings.BASE_URL,reverse('users:umowa', kwargs={'username': self.username})))

    @property
    def page_url(self):
        try:
            if self.user_type in (User.TYPES_FP, ):
                the_url = self.foodprovider_set.first().url
            elif self.user_type in (User.TYPES_FD, ):
                the_url = self.fooddistributor_set.first().url
            elif self.user_type in (User.TYPES_FDS, ):
                the_url = self.fooddistributorshop_set.first().url
            else:
                the_url = 'http://jemlokalnie.org'
        except AttributeError:
            the_url = 'http://jemlokalnie.org'
        return mark_safe("<a href='{}{}' target='_blank'>strona</a>".format(settings.BASE_URL,the_url))

    @property
    def contact(self):
        try:
            if self.user_type in (User.TYPES_FP, ):
                the_name = self.foodprovider_set.first().name
                the_url = self.foodprovider_set.first().url
            elif self.user_type in (User.TYPES_FD, ):
                the_name = self.fooddistributor_set.first().name
                the_url = self.fooddistributor_set.first().url
            elif self.user_type in (User.TYPES_FDS, ):
                the_name = self.fooddistributorshop_set.first().name
                the_url = self.fooddistributorshop_set.first().url
            else:
                the_name = 'jemlokalnie.org'
                the_url = 'http://jemlokalnie.org'
        except AttributeError:
            the_name = 'jemlokalnie.org'
            the_url = 'http://jemlokalnie.org'

        return {
            'name': the_name,
            'phone_number': self.phone,
            'email': self.email,
            'url':  "http://jemlokalnie.org{}".format(the_url),
            'company': self.username,
        }


    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

        if(self.user_type == User.TYPES_FD and self.fooddistributor_set.count() == 0):
            logger.debug("Misssing restaurator - creating")
            foodX = self.fooddistributor_set.create( name = self.username)
            super(User, self).save(*args, **kwargs)

        if(self.user_type == User.TYPES_FDS and self.fooddistributorshop_set.count() == 0):
            logger.debug("Misssing shop - creating")
            foodX = self.fooddistributorshop_set.create( name = self.username)
            super(User, self).save(*args, **kwargs)

        if(self.user_type == User.TYPES_FP and self.foodprovider_set.count() == 0):
            logger.debug("Misssing provider - creating")
            foodX = self.foodprovider_set.create( name = self.username)
            super(User, self).save(*args, **kwargs)

    @property
    def verifiedEmail(self):
        ret = False
        try:
            ret = self.emailaddress_set.all()[0].verified
            # ret = dir(self)
        except:
            pass
        return ret




class EmailOrUsernameModelBackend(object):
    def authenticate(self, username=None, password=None):
        user = None
        try:
            kwargs  = {'username': username }
            logger.debug('ssssssssssssss')
            user    = User.objects.get(**kwargs)

        except User.DoesNotExist:
            if '@' in username:
            	logger.debug('ssssssssssssss')
                try:
                    kwargs = {'email': username }
                    user = User.objects.get(**kwargs)
                except User.DoesNotExist:
                    return None
            else:
                return None

        if user.check_password(password):
            return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None





