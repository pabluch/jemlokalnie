﻿import logging, sys, datetime, dateutil.parser

from django.conf import settings
from django.db import models
from django.db.models import Q,F,Count, signals
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, reverse_lazy


logger = logging.getLogger( settings.DEFAULT_LOGGER )