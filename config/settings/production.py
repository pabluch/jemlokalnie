﻿"""
Local settings

- Run in Debug mode

- Use console backend for emails

- Add Django Debug Toolbar
- Add django-extensions as app
"""

from .base import *  # noqa

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = False
#env.bool('DJANGO_DEBUG', default=True)
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
# Mail settings
# ------------------------------------------------------------------------------

#EMAIL_PORT = 1025
#EMAIL_HOST = 'localhost'
#EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')

EMAIL_HOST="localhost"
EMAIL_USE_TLS=1
EMAIL_PORT=587
EMAIL_HOST_USER='biuro@jemlokalnie.org'
EMAIL_HOST_PASSWORD='!demon666!'
DEFAULT_FROM_EMAIL = 'biuro@jemlokalnie.org'
SERVER_EMAIL = 'biuro@jemlokalnie.org'
MANAGERS = [('Pablo', 'pablo.lukaszewicz@gmail.com'), ('Biuro', 'biuro@jemlokalnie.org')]
ADMINS = [('Pablo', 'pablo.lukaszewicz@gmail.com'), ]
#EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_FILE_PATH = '/home/pabloble/domains/jemlokalnie.org/public_html/jemlokalnie/jemlokalnie_src/logs/emails'

# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# django-debug-toolbar
# ------------------------------------------------------------------------------
#MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]
#INSTALLED_APPS += ['debug_toolbar', ]

INTERNAL_IPS = ['127.0.0.1', '10.0.2.2', ]

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}

BASE_URL="http://jemlokalnie.org"
STATIC_URL = '/static/'
# django-extensions
# ------------------------------------------------------------------------------
INSTALLED_APPS += ['django_extensions', ]

# TESTING
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

########## CELERY
# In development, all tasks will be executed locally by blocking until the task returns
CELERY_ALWAYS_EAGER = True
########## END CELERY

ALLOWED_HOSTS = [u'portal.jemlokalnie.org',u'jemlokalnie.org',u'www.jemlokalnie.org']

# Your local stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------
