"""
WSGI config for jemlokalnie project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
from datetime import datetime

sys.path.insert(0, os.path.dirname(__file__))
from django.core.wsgi import get_wsgi_application


app_path = os.path.dirname(os.path.abspath(__file__)).replace('/config', '')
sys.path.append(os.path.join(app_path, 'jemlokalnie'))
sys.path.append("/home/pabloble/domains/jemlokalnie.org/public_html/jemlokalnie/jemlokalnie_src")

os.environ['DJANGO_JEMLOKALNIE_SECRET_KEY']="9-CeCjkpfffdhb]sRkkDtdgsq4c&Je,gLm^Iw3g|%1A.ZA|[q.d"
os.environ['DJANGO_SETTINGS_MODULE']="config.settings.production"
os.environ['DATABASE_USR']="pabloble_jemloka"
os.environ['DATABASE_NAME']="pabloble_jemloka"
os.environ['DATABASE_PASS']="997DupaChuj!!!"


if os.environ.get('DJANGO_SETTINGS_MODULE') == 'config.settings.production':
    from raven.contrib.django.raven_compat.middleware.wsgi import Sentry

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

application = get_wsgi_application()
if os.environ.get('DJANGO_SETTINGS_MODULE') == 'config.settings.production':
    application = Sentry(application)



# def application(environ, start_response):
#     now = datetime.now()
#     current_time = now.strftime("%H:%M:%S")
#     start_response('200 OK', [('Content-Type', 'text/plain')])
#     message = 'It works!\n'
#     version = 'Python %s\n' % sys.version.split()[0]
#     response = '\n'.join([message, version,  os.environ.get('DJANGO_SETTINGS_MODULE',"jprdl"), current_time])
#     return [response.encode()] 